import {App} from "./App"
import {Smartphone} from "./Smartphone";

export class PhotoApps extends App{

cameraProperties = {"state": false,
                    "whichCamera" : "frontal"};

    openCamera(){
        if (this.Appstate){
            this.cameraProperties.state = true;
            return "Camera is On";

        }else{
            return "The app haven't been inicialized";}

    }

    switchCamera(){
        if (this.Appstate && this.cameraProperties.state){
            if(this.cameraProperties.whichCamera == "frontal") {
                this.cameraProperties.whichCamera = "rear";

            }else{
                this.cameraProperties.whichCamera = "frontal";
            }

        }else{
            return "The app haven't been inicialized";}
    }

    openGallery(){
        if (this.Appstate ){
            this.StartApp();
            return "open Gallery";
        }else{
            return "The app haven't been inicialized";}

    }

    exitGalery(){
        if (this.Appstate ){
            this.StopApp();
            return "Close Gallery";
        }else{
            return "The app haven't been inicialized";}
    }
}

export class photo{
    photo: string;
    date: Date;
    format: string;

    constructor(inputPhoto:string, inputDate: Date, inputFormat:string = ".jpg") {
        this.photo = inputPhoto;
        this.date = inputDate;
        this.format = inputFormat;
    }
}

export let photosInCloudOrLocalStorage: photo[] = [new photo("🤓", new Date()),
                                     new photo("🥳", new Date()),
                                     new photo("👻", new Date()),
                                     new photo("🧔", new Date()),
                                     new photo("🧓", new Date()),
                                     new photo("🤖", new Date()),
                                     new photo("👲", new Date()),
                                     new photo("🏋", new Date()),
                                     new photo("👸", new Date())]