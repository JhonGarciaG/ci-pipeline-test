export {App}

abstract class App {


    name:string;
    releaseDate: Date;
    UseTime : number;
    Platform : string;
    category : string;
    Appstate : boolean;

    constructor(name, category, Appstate = false) {
        this.name = name;
        this.category = category;
        this.Appstate = Appstate;
    }

    StartApp(){
    }

    StopApp(){
    }

}