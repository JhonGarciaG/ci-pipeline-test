import {App} from "./App";
import {Smartphone} from "./Smartphone";

export class MusicApps extends App{

    searchMusic(musicNameInput:string){
        // First verify the app have been inicialized
       if(this.Appstate == true){

           let musicExist: music = musicInCloudOrLocalStorage.find(music => music.name == musicNameInput )
            if(musicExist){
                return "Music exist !!"}
            else{
                return "Music doesn't exist"}
        }else{
            return "the app haven't been inicialized"
        }
    }

    playMusic(musicNameInput:string){
       if(this.Appstate == true){

           let musicExist: music = musicInCloudOrLocalStorage.find(music => music.name == musicNameInput );

            if(musicExist){
                let musicIndex = musicInCloudOrLocalStorage.findIndex(music => music.name == musicNameInput);
                musicInCloudOrLocalStorage[musicIndex].state = true;

                return "Playing the song " + musicNameInput + " !!"}
            else{
                return "Music doesn't exist"}

        }else{
            return "the app haven't been inicialized"
        }

    }

    AddToPlayList(musicInput: music){
        let validInput:boolean = (typeof musicInput === "object");
        if(!validInput){
            return "The file isn't a song"
        }
        if(this.Appstate == true){

           let musicExist: music = musicInCloudOrLocalStorage.find(music => music.name == musicInput.name );

            if(musicExist){
                return "Music already exist in the Playlist !!!"}
            else{
                musicInCloudOrLocalStorage.push(musicInput)
                return "The song " + musicInput.name + " was added successfully !!"}

        }else{
            return "the app haven't been inicialized" }

    }

    DeleteFromPlayList(musicNameInput:string){
        let validInput:boolean = (typeof musicNameInput === "string");
        if(!validInput){
            return "Invalid name"
        }

        if(this.Appstate == true){
           let musicExist: music = musicInCloudOrLocalStorage.find(music => music.name == musicNameInput );

            if(musicExist){
                let musicIndex = musicInCloudOrLocalStorage.findIndex(music => music.name == musicNameInput);
                musicInCloudOrLocalStorage.splice(musicIndex,1)
                return "The song '" + musicNameInput + "' was deleted successfully !!"}
            else{
                return "Music doesn't exist in the Playlist !!! "}

        }else{
            return "the app haven't been inicialized" }

        }

}

export class music{
    name: string;
    artist: string;
    state : boolean;

    constructor(inputName:string, inputArtist:string, inputState:boolean = false) {
        this.name = inputName;
        this.artist = inputArtist;
        this.state = inputState;
    }
}

export let musicInCloudOrLocalStorage:music[]= [new music("A Dios le pido", "Juanes"),
                            new music("A puro dolor", "Son by Four"),
                            new music("Ahora quién", "Marc Anthony"),
                            new music("Amigo", "Roberto Carlos"),
                            new music("Amor prohibido", "Selena"),
                            new music("Bailando", "Enrique Iglesias y Gente de Zona"),
                            new music("Bésame mucho", "varios artistas"),
                            new music("Burbujas de amor", "Juan Luis Guerra"),
                            new music("Cali Pachanguero", "Grupo Niche"),
                            new music("Color esperanza", "Diego Torres")]