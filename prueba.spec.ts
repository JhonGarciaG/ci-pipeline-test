import {Smartphone} from "./Smartphone";
import {initialsApps, appBank} from "./AppsBank";
import {App} from "./App";
import {music, MusicApps, musicInCloudOrLocalStorage} from "./MusicApps";
import {message, MessagesApps, messagesInCloudOrLocalStorage} from "./MessagesApps";
import {PhotoApps} from "./PhotoApps";


// Instance of Smartphone object
let JhonSmartphone: Smartphone = new Smartphone("MotoE4", "Android", initialsApps);


describe("\n\n-.__.--.__.--.__.--.__.--.__.--.__.-\n" +
         "   TESTING THE SMARTPHONE CLASS\n" +
         "-.__.--.__.--.__.--.__.--.__.--.__.- ", function() {

    test("1.1)Instance of Smartphone", function () {
        expect(JhonSmartphone).toBeInstanceOf(Smartphone);
    })

    test("1.2)Verify the initial powerState of Smartphone", function () {
        expect(JhonSmartphone.powerState).toBe(false);
    })


    test("1.3)Power On the cellphone", function () {
        JhonSmartphone.turnOn()
        expect(JhonSmartphone.powerState).toBeTruthy();
    })

    test("1.4)Testing the installed app by default", function () {
        for (let iter = 0; iter < JhonSmartphone.installedApps.length; iter++) {
            expect(JhonSmartphone.installedApps[iter]).toBeInstanceOf(App);
        }
    })


    let nameApp: string = "Whatsapp";
    test("1.5) Download a App from the Appstore", function () {
        let installedApp = JhonSmartphone.downloadAndInstallApp(nameApp, appBank);
        expect(installedApp).toBeInstanceOf(App);
    })


    let AppDoesntExist: string = "Whap";

    test("1.6) Don't download a App that doesn't exist in the app store", function () {
        expect(JhonSmartphone.downloadAndInstallApp(AppDoesntExist, appBank)).toBe("the App Doesn't exist in Store");
    })

    // @ts-ignore
    test("1.7) Open a Application", function () {
        let indexApp = JhonSmartphone.findIndexByNumber(nameApp);
        JhonSmartphone.openApp(nameApp)
        // @ts-ignore
        expect(JhonSmartphone.installedApps[indexApp].Appstate).toBe(true);

        // @ts-ignore
        expect(JhonSmartphone.installedApps[indexApp]).toBeInstanceOf(App);
    })

    test("1.8) Close the App", function () {
        // https://www.tutorialsteacher.com/typescript/type-assertion
        let indexApp = <number>JhonSmartphone.findIndexByNumber(nameApp);
        JhonSmartphone.closeApp(nameApp)
        expect(JhonSmartphone.installedApps[indexApp].Appstate).toBe(false);
        expect(JhonSmartphone.installedApps[indexApp]).toBeInstanceOf(App);

    })

    test("1.9) uninstall App ", function () {
        // checking the state of App before uninstall it
        /*@ts-ignore
        let indexApp:number = JhonSmartphone.findIndexByNumber(nameApp);*/

        let indexApp = <number>JhonSmartphone.findIndexByNumber(nameApp);
        let appInstance = JhonSmartphone.installedApps[indexApp];

        expect(JhonSmartphone.installedApps.includes(appInstance)).toBe(true);

        // Uninstalling the app
        JhonSmartphone.uninstallApp(nameApp)

        // checking the state of App before uninstall it
        // @ts-ignore
        expect(JhonSmartphone.installedApps.includes(appInstance)).toBe(false);

    });

    test("1.10) Turn off the Smartphone", function () {
        JhonSmartphone.turnOff();
        expect(JhonSmartphone.powerState).toBe(false);
    });
});

/*describe("TESTING THE MUSIC APPS", function (){*/


describe("\n-.__.--.__.--.__.--.__.--.__.--.__.-\n" +
         "        TESTING THE MUSIC APPS         \n" +
         "-.__.--.__.--.__.--.__.--.__.--.__.-\n" ,function (){


    test("2.1) Search a Song that exists ",function (){
        // find a music app to test and open it
        //console.table(JhonSmartphone.installedApps);

        let instanceMusicApp =  <MusicApps>JhonSmartphone.installedApps.find(app => app.category == "music");

        // State of app before Open
        expect(instanceMusicApp.Appstate).toBeFalsy();

        JhonSmartphone.openApp(instanceMusicApp.name);
        expect(instanceMusicApp.searchMusic("Ahora quién")).toBe("Music exist !!");

        /*instanceMusicApp.name = "modified";

        let instance = JhonSmartphone.installedApps[appIndex].searchMusic("")
        console.table(JhonSmartphone.installedApps);
        console.log("->instanceMusicApp == JhonSmartphone /",instanceMusicApp == JhonSmartphone.installedApps[appIndex]);
        console.log("->instance == JhonSmartphone /",instance == JhonSmartphone.installedApps[appIndex]);
        console.log("->instance == instanceMusicAoo /",instance == instanceMusicApp);

        console.log("-------------------------------------------")
        console.log("->instanceMusicApp === JhonSmartphone /",instanceMusicApp === JhonSmartphone.installedApps[appIndex]);
        console.log("->instance === JhonSmartphone /",instance === JhonSmartphone.installedApps[appIndex]);
        console.log("->instance === instanceMusicA / ",instance === instanceMusicApp);

        //<MusicApps>JhonSmartphone.installedApps[appIndex].searchMusic("Ahora quién");*/


        // State of app before to
        expect(instanceMusicApp.Appstate).toBe(true);
    })

    test("2.2) Search a Song that doesn't exist",function (){
        //console.table(JhonSmartphone.installedApps);
        let indexApp = <number>JhonSmartphone.findIndexByNumber("Spotify");
        expect(JhonSmartphone.installedApps[indexApp].searchMusic("Olaaaaa")).toBe("Music doesn't exist")
    })

    test("2.3) Search a Song that exist with the App closed",function (){

        let indexApp = <number>JhonSmartphone.findIndexByNumber("Spotify");
        JhonSmartphone.closeApp("Spotify")

        expect(JhonSmartphone.installedApps[indexApp].searchMusic("Ahora quién")).toBe("the app haven't been inicialized")
    })

    test("2.3) Play a song that exist",function (){

        let indexApp = <number>JhonSmartphone.findIndexByNumber("Spotify");
        JhonSmartphone.openApp("Spotify");

        expect(JhonSmartphone.installedApps[indexApp].playMusic("A puro dolor")).toBe("Playing the song A puro dolor !!")
    })

    test("2.4) Play a song that doesn't exist",function (){

        let indexApp = <number>JhonSmartphone.findIndexByNumber("Spotify");
        JhonSmartphone.openApp("Spotify");

        expect(JhonSmartphone.installedApps[indexApp].playMusic("Cocorori")).toBe("Music doesn't exist")
    })

    test("2.5) Play a song the app closed",function (){

        let indexApp = <number>JhonSmartphone.findIndexByNumber("Spotify");
        JhonSmartphone.closeApp("Spotify");

        expect(JhonSmartphone.installedApps[indexApp].playMusic("A puro dolor")).toBe("the app haven't been inicialized")
    })

    test("2.6) Adding a new song to Playlist with the closed app",function (){

        let indexApp = <number>JhonSmartphone.findIndexByNumber("Spotify");
        let newSong =new music("Algo que se quede", "Grupo Niche");
        expect(JhonSmartphone.installedApps[indexApp].AddToPlayList(newSong)).toBe("the app haven't been inicialized");
    })

    test("2.7) Adding to Playlist a new song ",function (){

        let indexApp = <number>JhonSmartphone.findIndexByNumber("Spotify");
        JhonSmartphone.openApp("Spotify");
        let newSong =new music("Algo que se quede", "Grupo Niche");
        expect(JhonSmartphone.installedApps[indexApp].AddToPlayList(newSong)).toBe("The song Algo que se quede was added successfully !!");
    })

    test("2.8) Trying to add an  existent song to playlist",function (){

        let indexApp = <number>JhonSmartphone.findIndexByNumber("Spotify");
        JhonSmartphone.openApp("Spotify");
        let newSong =new music("Algo que se quede", "Grupo Niche");
        expect(JhonSmartphone.installedApps[indexApp].AddToPlayList(newSong)).toBe("Music already exist in the Playlist !!!");
    })

    test("2.9) Adding to Playlist a invalid file ",function (){

        let indexApp = <number>JhonSmartphone.findIndexByNumber("Spotify");
        expect(JhonSmartphone.installedApps[indexApp].AddToPlayList("Solecito")).toBe("The file isn't a song");
    })

    test("2.10) Removing a song from Playlist with the closed app",function (){

        let indexApp = <number>JhonSmartphone.findIndexByNumber("Spotify");
        JhonSmartphone.closeApp("Spotify");

        let newSong =new music("Algo que se quede", "Grupo Niche");
        expect(JhonSmartphone.installedApps[indexApp].AddToPlayList(newSong)).toBe("the app haven't been inicialized");
    })

    test("2.11) Removing a song from Playlist with the opened app",function (){

        let indexApp = <number>JhonSmartphone.findIndexByNumber("Spotify");
        let newSong =new music("Algo que se quede", "Grupo Niche");

        expect(JhonSmartphone.installedApps[indexApp].DeleteFromPlayList(newSong.name)).toBe("the app haven't been inicialized");
        JhonSmartphone.openApp("Spotify");

        expect(JhonSmartphone.installedApps[indexApp].DeleteFromPlayList(newSong)).toBe("Invalid name");
        expect(JhonSmartphone.installedApps[indexApp].DeleteFromPlayList(newSong.name)).toBe("The song '"+ newSong.name + "' was deleted successfully !!");
        expect(JhonSmartphone.installedApps[indexApp].DeleteFromPlayList(newSong.name)).toBe("Music doesn't exist in the Playlist !!! ");
    })

})

describe("\n\n-.__.--.__.--.__.--.__.--.__.--.__.-\n" +
         "   TESTING THE PHOTOAPP CLASS\n" +
         "-.__.--.__.--.__.--.__.--.__.--.__.- ", function() {
    let instancePhotoApp =  <PhotoApps>JhonSmartphone.installedApps.find(app => app.category == "photo");
    let indexApp = <number>JhonSmartphone.findIndexByNumber(instancePhotoApp.name);

    test("3.1) Checking the initial Camera state (false)", function () {
        expect(instancePhotoApp.cameraProperties.whichCamera).toBe("frontal");
        expect(instancePhotoApp.cameraProperties.state).toBe(false);
    })

    test("3.2) Open Camera", function (){

        expect(instancePhotoApp.cameraProperties.state).toBe(false);
        expect(instancePhotoApp.openCamera()).toBe("The app haven't been inicialized");        // App closed

        JhonSmartphone.openApp(instancePhotoApp.name);
        let cameraState = instancePhotoApp.openCamera();
        expect(cameraState).toBe("Camera is On");
        expect(instancePhotoApp.cameraProperties.state).toBe(true);
    })

    test("3.3) Switch camera", function (){
        // switch the initial state from "frontal" to "rear"
        expect(instancePhotoApp.cameraProperties.whichCamera).toBe("frontal");
        instancePhotoApp.openCamera();
        instancePhotoApp.switchCamera();
        expect(instancePhotoApp.cameraProperties.state).toBe(true);
        expect(instancePhotoApp.Appstate).toBe(true);
        expect(instancePhotoApp.cameraProperties.state).toBe(true);
        expect(instancePhotoApp.cameraProperties.whichCamera).toBe("rear");

        // switch the initial state from "rear" to "frontal"
        instancePhotoApp.switchCamera();
        expect(instancePhotoApp.cameraProperties.whichCamera).toBe("frontal");

    });

    test("3.4) Open Gallery", function (){

        JhonSmartphone.openApp(instancePhotoApp.name);
        expect(instancePhotoApp.Appstate).toBe(true);
        expect(instancePhotoApp.openGallery()).toBe("open Gallery")

        JhonSmartphone.closeApp(instancePhotoApp.name);
        expect(instancePhotoApp.Appstate).toBe(false);
        expect(instancePhotoApp.openGallery()).toBe("The app haven't been inicialized");

    })

    test("3.5) Exit Gallery", function (){
        expect(instancePhotoApp.Appstate).toBe(false);

        // exitGalery without initialize the App
        expect(instancePhotoApp.exitGalery()).toBe("The app haven't been inicialized")

        JhonSmartphone.openApp(instancePhotoApp.name);
        expect(instancePhotoApp.exitGalery()).toBe("Close Gallery")


    });

});

 describe("\n\n-.__.--.__.--.__.--.__.--.__.--.__.-\n" +
          "   TESTING THE MESSAGEAPP CLASS\n" +
          "-.__.--.__.--.__.--.__.--.__.--.__.- ", function() {
     let instanceMessageApp =  <MessagesApps>JhonSmartphone.installedApps.find(app => app.category == "message");
     let indexApp = <number>JhonSmartphone.findIndexByNumber(instanceMessageApp.name);
     //data to create write a message
     let from: string = "empleado1@lsv-tech.com",
         to: string = "empleado2@lsv-tech.com",
         subject: string = "Importat meeting",
         content: string = " Would you help me to check the mockup today?",
         date: Date = new Date(),
         templateMesagge : message = new message(from, to, content,subject);
     test("4.1) Write a Email method doesn't work if the app isn't initilized ", function () {
         JhonSmartphone.turnOn();
         expect(JhonSmartphone.powerState).toBe(true);
         //state before initialite the app
         expect(instanceMessageApp.Appstate).toBe(false);
         expect(instanceMessageApp.WriteEmail(from, to, subject, content, date)).toBe("The app haven't been inicialized");
     });
     test("4.2) Write a Email method with the app initilized ", function (){
         JhonSmartphone.openApp(instanceMessageApp.name)
         //state after initialite the app
         expect(instanceMessageApp.Appstate).toBe(true);
         let createdMessage = instanceMessageApp.WriteEmail(from, to, subject, content, date); // create a message
         expect(createdMessage).toBe("Message created and saved");
         JhonSmartphone.closeApp(instanceMessageApp.name)});
     test("4.3) ReadEmail method doesn't work if the app isn't initilized ", function () {
         expect(JhonSmartphone.powerState).toBe(true);  // Checking that the Smartphone is On
         expect(instanceMessageApp.Appstate).toBe(false); //state before initialite the app
         expect(instanceMessageApp.ReadEmail(templateMesagge)).toBe("The app haven't been inicialized") });
     test("4.4) ReadEmail method with the app initilized ", function () {
         expect(JhonSmartphone.powerState).toBe(true);  // Checking that the Smartphone is On
         JhonSmartphone.openApp(instanceMessageApp.name) ;  // initializing the app
         expect(instanceMessageApp.Appstate).toBe(true); //state after initialite the app
         expect(instanceMessageApp.ReadEmail(templateMesagge)).toBe(console.table(templateMesagge))
         JhonSmartphone.closeApp(instanceMessageApp.name)});
     test("4.5) Delete method doesn't work if the app isn't initilized ", function () {
         expect(JhonSmartphone.powerState).toBe(true);  // Checking that the Smartphone is On
         expect(instanceMessageApp.Appstate).toBe(false); //state before initialite the app
         expect(instanceMessageApp.DeleteEmail(templateMesagge)).toBe("The app haven't been inicialized") });

     test("4.5) Delete method with the App initilized ", function () {
         expect(JhonSmartphone.powerState).toBe(true);  // Checking that the Smartphone is On
         JhonSmartphone.openApp(instanceMessageApp.name) ;  // initializing the app
         expect(instanceMessageApp.Appstate).toBe(true); //state after initialite the app
         let lastElment = messagesInCloudOrLocalStorage[messagesInCloudOrLocalStorage.length -1];
         expect(messagesInCloudOrLocalStorage.includes(lastElment)).toBe(true); // Checking that the message exist before delete it
         instanceMessageApp.DeleteEmail(lastElment)
         expect(messagesInCloudOrLocalStorage.includes(lastElment)).toBe(false)}); // Checking that the message doesn't exist after delete it

});
