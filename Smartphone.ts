import {App} from "./App";
import {photo, PhotoApps, photosInCloudOrLocalStorage} from "./PhotoApps";
import {music, MusicApps, musicInCloudOrLocalStorage} from "./MusicApps";
import {message, MessagesApps, messagesInCloudOrLocalStorage} from "./MessagesApps";


export class Smartphone{
    readonly model: String
    readonly releaseDay: Date;
    readonly SO: String;
    public installedApps:any[] //MusicApps[] | PhotoApps[] | MessagesApps[] ;
    public powerState : boolean;
    public musicInLocalStorage : music[] = musicInCloudOrLocalStorage;
    public photosInLocalStorage : photo[] = photosInCloudOrLocalStorage;
    public messagesInLocalStorage : message[] = messagesInCloudOrLocalStorage;

    //installedApps?: App[];

    constructor(model:string, SO:string, initialsApps:App[]) {
        this.model= model;
        this.releaseDay = new Date();
        this.SO = SO;
        this.installedApps = initialsApps;
        this.powerState = false;
    }

    turnOn(){
        this.powerState = true;
        //console.log("The cellphone is ON")
    }

    turnOff(){
        this.powerState = false
        //console.log("The cellphone is OFF")
    }


    private validateInstalledApp(appName:string, returnName:string){
        //Search in the phone's local stores
        let searchInPhone: App = (this.installedApps.find(app => app.name == appName));
        let index:number = this.installedApps.indexOf(searchInPhone);


        if(returnName === "boolean" && searchInPhone != undefined && searchInPhone.name == appName){
            return true;
        }
        else if(returnName === "index"){
            return index;

        }else{
            //console.log("The app isn't installed ")
            return false;
        }
    }

    public findIndexByNumber(appName:string){
        return (this.validateInstalledApp(appName, "index"));
    }

    downloadAndInstallApp(appName:string, appBank:App[]) {
        // Search in the PlayStore
        // @ts-ignore
        let searchInStore: App = (appBank.find(app => app.name == appName))

        if (this.validateInstalledApp(appName,"boolean")) {
            // condition if the App already exist
            console.log("the App already installed")

        }else{

            if (searchInStore == undefined) {
                //console.log("the App Doesn't exist in Store")
                return "the App Doesn't exist in Store" ;
            } else {
                // condition if the app doesn't installed
                let AppIndex: number = appBank.indexOf(searchInStore);
                this.installedApps.push(appBank[AppIndex]);

                 return appBank[AppIndex];
            }

        }
    }

    private changeStateApp(appName:string, desireState:boolean){
        if (this.validateInstalledApp(appName, "boolean")){
            let appIndex = this.validateInstalledApp(appName, "index");
            // @ts-ignore
            this.installedApps[appIndex].Appstate = desireState;
        }
    }

    openApp(appName:string){
        let appIndex = this.validateInstalledApp(appName, "index");
        this.changeStateApp(appName,true);
        // @ts-ignore
        return this.installedApps[appIndex]
    }

    closeApp(appName:string){
        this.changeStateApp(appName,false);
    }

    uninstallApp(appName:string){
        // return true if the app exist
        if ( this.validateInstalledApp(appName, "boolean")){
            let appIndex = this.validateInstalledApp(appName, "index");
            // @ts-ignore
            this.installedApps.splice(appIndex,1)
        }
    }

}

