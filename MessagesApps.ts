import {App} from "./App";

export class MessagesApps extends App{

    WriteEmail(from:string, to:string, subject:string, content:string, date:Date ){
        if(this.Appstate) {
            let messageCreated = new message(from, to, content, subject);
            messagesInCloudOrLocalStorage.push(messageCreated);
            return "Message created and saved";

        }else{
            return "The app haven't been inicialized";
        }
    }

    ReadEmail(inputMessage: message){
        if(this.Appstate){
            return console.table(inputMessage);
        }else {
            return "The app haven't been inicialized"
        }
    }

    DeleteEmail(inputMessage: message){
        if(this.Appstate){
            let emailIndex = messagesInCloudOrLocalStorage.findIndex(message => message == inputMessage);
            if(emailIndex != undefined){
                messagesInCloudOrLocalStorage.splice(emailIndex,1);}
        }else{
            return "The app haven't been inicialized";
        }
    }

}

export class message{
    from: string;
    to: string;
    subject: string;
    content : string;
    date : Date ;

    constructor(fromInput:string, toInput:string, contentInput:string, subjectInput:string = "") {
        this.from = fromInput;
        this.to = toInput;
        this.content = contentInput;
        this.subject = subjectInput;
        this.date = new Date();
    }
}



export let messagesInCloudOrLocalStorage: message[] = [new message("empleado1@lsv-tech.com", "empleado2@lsv-tech.com","FYI"),
                                         new message("empleado2@lsv-tech.com", "empleado1@lsv-tech.com","Reply"),
                                         new message("empleado1@lsv-tech.com", "empleado2@lsv-tech.com","Reply2"),
                                         new message("empleado2@lsv-tech.com", "empleado1@lsv-tech.com","Reply3"),]