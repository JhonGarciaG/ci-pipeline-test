import {App} from "./App";
import {MusicApps} from "./MusicApps";
import {PhotoApps} from "./PhotoApps";
import {MessagesApps} from "./MessagesApps";


export let appBank = [new MusicApps("Spotify","music"),
                            new MessagesApps("SMS","message"),
                            new PhotoApps("Google photos", "photo"),
                            new PhotoApps("Camera", "photo"),
                            new PhotoApps("Gallery","photo"),
                            new MusicApps("Claro Music", "music"),
                            new MusicApps("Tigo Music", "music"),
                            new MusicApps("Movistar Music", "music"),
                            new MusicApps("Amazon Music", "music"),
                            new MusicApps("Pandora Music", "music"),
                            new MusicApps("Groove", "music"),
                            new MusicApps("Radio", "music"),
                            new PhotoApps("Facebook", "photo"),
                            new PhotoApps("Instagram", "photo"),
                            new PhotoApps("Photo editor", "photo"),
                            new MessagesApps("Whatsapp", "message"),
                            new MessagesApps("Telegram", "message"),
                            new MessagesApps("Signal", "message"),
                            new MessagesApps("Wechat", "message")]

export let initialsApps = [appBank[0], appBank[1], appBank[2], appBank[3],appBank[4]];

/*console.log(initialsApps[0]["name"])

// @ts-ignore
let filtro: App= (appBank.find(app => app.name =="Claro Music"))
console.log(filtro == undefined)
console.log(filtro.name == "Claro Music")

console.log(appBank.indexOf(filtro));

// @ts-ignore*/
